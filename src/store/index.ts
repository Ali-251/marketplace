import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createLogger } from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import rootSaga from '../sagas';
import rootReducer from './../reducers';

const sagaMiddleware = createSagaMiddleware();
const AppStore = createStore(rootReducer, composeWithDevTools(applyMiddleware(sagaMiddleware, createLogger())));

sagaMiddleware.run(rootSaga);
export default AppStore;
