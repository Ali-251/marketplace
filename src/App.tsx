import { Container } from '@material-ui/core';
import React, { createContext, useState } from 'react';
import './App.css';
import Home from './containers/Home';

export const MyContext = createContext({ state: 'peter', setState: () => {} }) as any;

function App() {
  const [state, setState] = useState('');

  return (
    <div className='App'>
      <MyContext.Provider value={{ state, setState }}>
        <Home />
      </MyContext.Provider>
    </div>
  );
}

export default App;
