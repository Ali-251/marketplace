import { SagaIterator } from 'redux-saga';
import { all, fork, call, takeLatest } from 'redux-saga/effects';
import { GET_BEST_ACCOUNTS_REQUEST } from '../actions';
import { apiGetAccounts } from '../api';

function* bestAccountsSaga() {
  yield call(apiGetAccounts);
}

function* watchAccountActions(): any {
  yield takeLatest(GET_BEST_ACCOUNTS_REQUEST, bestAccountsSaga);
}

export default function* rootSaga(): any {
  yield all([fork(watchAccountActions)]);
}
