import { gql } from '@apollo/client';

export const GET_ACCOUNT = gql`
  query getAccount {
    accounts {
      name
      remote_id
      address {
        suburb
        street
        postCode
      }
    }
    categories {
      id
      name
    }
  }
`;

export const FILTERED_SEARCH = gql`
  query($category: String, $brand: String, $postcode: String) {
    getFilteredAccounts(category: $category, brand: $brand, postcode: $postcode) {
      name
    }
    categories {
      id
      name
    }
  }
`;
