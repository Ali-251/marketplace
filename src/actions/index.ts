export const GET_BEST_ACCOUNTS_REQUEST = 'BEST_ACCOUNTS_REQUEST';
export const GET_BEST_ACCOUNTS_SUCCESS = 'BEST_ACCOUNTS_SUCCESS';
export const GET_BEST_ACCOUNTS_ERROR = 'BEST_ACCOUNTS_ERROR';

export const getBestAccountsRequest = payload => {
  return {
    type: GET_BEST_ACCOUNTS_REQUEST,
    payload,
  };
};

export const GetBestAccountsRequestSuccess = payload => {
  return {
    type: GET_BEST_ACCOUNTS_REQUEST,
    payload,
  };
};

export const getBestAccountsRequestError = payload => {
  return {
    type: GET_BEST_ACCOUNTS_REQUEST,
    payload,
  };
};
