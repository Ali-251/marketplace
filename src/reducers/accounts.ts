import { GET_BEST_ACCOUNTS_REQUEST, GET_BEST_ACCOUNTS_ERROR, GET_BEST_ACCOUNTS_SUCCESS } from '../actions';

const initialState = {
  accounts: [],
  isLoading: false,
  error: '',
};

const accountReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_BEST_ACCOUNTS_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case GET_BEST_ACCOUNTS_SUCCESS:
      return {
        ...initialState,
        accounts: action.payload,
      };
    case GET_BEST_ACCOUNTS_ERROR:
      return {
        ...state,
        isLoading: false,
        error: action.payload,
      };
    default:
      return state;
  }
};

export default accountReducer;
