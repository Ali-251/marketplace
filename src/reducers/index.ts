import { combineReducers } from 'redux';
import accountReducer from './accounts';

const rootReducer = combineReducers({
  accounts: accountReducer,
});
export default rootReducer;
