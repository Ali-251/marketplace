import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { Provider } from 'react-redux';
import { ApolloClient, InMemoryCache, ApolloProvider } from '@apollo/client';
import { GRAPHQL_END_POINT } from './config';
import AppStore from './store';

const client = new ApolloClient({
  uri: GRAPHQL_END_POINT,
  cache: new InMemoryCache(),
});
ReactDOM.render(
  <React.StrictMode>
    <Provider store={AppStore}>
      <ApolloProvider client={client}>
        <App />
      </ApolloProvider>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root'),
);

reportWebVitals();
