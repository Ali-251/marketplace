import React, { useState, useContext } from 'react';
import { Button, Container, Grid, Paper } from '@material-ui/core';
import MultiSelect from '../../components/MultiSelect';
import PropTypes from 'prop-types';
import { FILTERED_SEARCH } from '../../queries';
import { getApolloClient } from '../../graphql/client';
const SearchFilters = ({ categories, brands, postcodes, handleSubmit }) => {
  const [selectedCategory, setSelectedCategory] = useState(null);
  const [selectedBrand, setSelectedBrand] = useState(null);
  const [selectedPostcode, setSelectedPostcode] = useState(null);

  const handleChangeCategory = event => {
    setSelectedCategory(event.target.value);
  };
  const handleChangeBrand = event => {
    setSelectedBrand(event.target.value);
  };
  const handleChangePostcode = event => {
    setSelectedPostcode(event.target.value);
  };

  function prepareForSubmit() {
    const finalPayload = {
      category: selectedCategory,
      brand: selectedBrand,
      postcode: selectedPostcode,
    };
    handleSubmit(finalPayload);
  }

  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'center',
        justifyItems: 'center',
        minWidth: '100%',
        padding: '30px 0px',
        // border: '2px solid red',
        background: ' #f3f5f6',
        margin: '50px 0px',
      }}
    >
      <Container>
        <Grid container className='search-filters' direction='row' justify='center' alignItems='center' spacing={3}>
          <Grid item xs={6} sm={6} md={3}>
            <MultiSelect
              options={postcodes}
              label={'Postcode'}
              handleChange={handleChangePostcode}
              selectedValue={selectedPostcode}
            />
          </Grid>
          <Grid item xs={6} sm={6} md={3}>
            <MultiSelect
              options={brands}
              label={'Brands'}
              handleChange={handleChangeBrand}
              selectedValue={selectedBrand}
            />
          </Grid>
          <Grid item xs={6} sm={6} md={3}>
            <MultiSelect
              options={categories}
              label={'Categories'}
              handleChange={handleChangeCategory}
              selectedValue={selectedCategory}
            />
          </Grid>
          <Grid item xs={6} sm={6} md={3}>
            <div style={{ display: 'flex', justifyContent: 'center', height: '50px', width: '100%' }}>
              <Button
                color='primary'
                variant='contained'
                onClick={prepareForSubmit}
                style={{
                  width: '100%',
                  height: '100%',
                  marginTop: '7px',
                }}
              >
                Search
              </Button>
            </div>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
};

SearchFilters.propTypes = {
  categories: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      id: PropTypes.string,
    }).isRequired,
  ),
  brands: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      id: PropTypes.string,
    }).isRequired,
  ),
  postcodes: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      id: PropTypes.string,
    }).isRequired,
  ),

  handleSubmit: PropTypes.func.isRequired,
};

export default SearchFilters;
