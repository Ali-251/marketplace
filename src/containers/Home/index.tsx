import React, { useEffect, useState } from 'react';
import SearchFilters from './SearchFilters';
import Account from '../../components/Account';
import { Button, Container, Grid } from '@material-ui/core';
import { getBestAccountsRequest } from '../../actions';
import { connect } from 'react-redux';
import { GET_ACCOUNT } from '../../queries';

const Home = props => {
  console.log('props =>', props);
  const { accountsx, topAccounts, isLoading } = props;
  const renderLoading = () => {
    return <p>loading...</p>;
  };
  useEffect(() => {
    console.log('called');
    // function getTopAccounts() {}
    // getTopAccounts();
  }, []);
  const accounts = [
    {
      name: 'solar installer',
    },
    {
      name: 'Carpet call pty',
    },
    {
      name: 'solar installer',
    },
    {
      name: 'Carpet call pty',
    },
  ];

  if (isLoading) return renderLoading();

  const categories = [{ id: '1', name: 'some category' }];

  return (
    <div>
      <SearchFilters
        brands={categories}
        postcodes={categories}
        categories={categories}
        handleSubmit={() => console.log('called')}
      />
      <Container>
        <Grid item sm={12}>
          <Grid container></Grid>
          <Grid container spacing={3}>
            {accounts.map((account, index) => {
              return (
                <Grid item sm={12} xs={12} md={6} key={index}>
                  <Account account={account} />
                </Grid>
              );
            })}
          </Grid>
        </Grid>
      </Container>
    </div>
  );
};

const mapDispatchToProps = () => {
  return {
    topAccounts: () => getBestAccountsRequest(GET_ACCOUNT),
  };
};

const mapStateToProps = state => {
  return {
    isLoading: state.accounts.isLoading,
    accounts: state.accounts.accounts,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
