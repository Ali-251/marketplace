import { getApolloClient } from '../graphql/client';
import { GET_ACCOUNT } from '../queries';

export const apiGetAccounts = () => {
  return getApolloClient().query({
    query: GET_ACCOUNT,
  });
};
