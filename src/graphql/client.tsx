import { GRAPHQL_END_POINT } from '../config';
import { ApolloClient, InMemoryCache } from '@apollo/client';

export const getApolloClient = () => {
  return new ApolloClient({
    uri: GRAPHQL_END_POINT,
    cache: new InMemoryCache(),
  });
};
