import React from 'react';

import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import { Grid } from '@material-ui/core';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const StyledMultiSelect = styled(Select)``;
const MultiSelect = ({ selectedValue, options, label, handleChange }) => {
  return (
    <>
      <InputLabel>{label}</InputLabel>
      <StyledMultiSelect value={selectedValue} onChange={handleChange} fullWidth variant='outlined'>
        {options.map((cat, index) => (
          <MenuItem key={index} value={cat.name}>
            {cat.name}
          </MenuItem>
        ))}
      </StyledMultiSelect>
    </>
  );
};

MultiSelect.propTypes = {
  options: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      id: PropTypes.string,
    }),
  ),
  label: PropTypes.string,
  handleChange: PropTypes.func,
  selectedValue: PropTypes.string,
};

export default MultiSelect;
