import { Button, Card, CardContent, Grid, Link, Typography } from '@material-ui/core';
import React from 'react';
import CategoryList from './Account/CategoryList';
import RecentJobs from './Account/RecentJobs';
import styled from 'styled-components';

const ViewLink = styled(Link)`
  border: 2px solid #00d280;
  width: 40%;
  border-radius: 15px;
  padding: 10px;
  margin: 10px 0px 0px 0px;
  color: black;

  &:hover {
    transform: scale(1.02, 1.05);
    cursor: pointer;
  }
`;

export default function Account({ account }) {
  return (
    <Card raised={true} elevation={10} className='account'>
      <CardContent>
        <Grid container>
          <Grid item sm={12}>
            <Grid container>
              <Grid item style={{}} xs={5} sm={3} md={3}>
                <img
                  src='https://s3-ap-southeast-2.amazonaws.com/staging-portal-20200723070853721200000001/public/vendors/3-test1-ff7b08d5bfb61407ad489aa2b04fcf561ca06513.jpg'
                  alt='brighte vendor'
                  style={{ height: '100px', width: 'auto' }}
                ></img>
              </Grid>
              <Grid item xs={7} sm={9} md={9}>
                <div>
                  <div>
                    <Typography variant='h5' component='h1'>
                      {account.name || 'Eco installer'}
                    </Typography>
                  </div>
                  <CategoryList />
                </div>
              </Grid>
            </Grid>
          </Grid>
          <Grid item sm={12}>
            <div>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
                deserunt mollit anim id est laborum.
              </p>
            </div>
          </Grid>
          <Grid item sm={12}>
            <RecentJobs />
          </Grid>
          <Grid item xs={12} sm={12} md={12}>
            <div style={{ display: 'flex', justifyContent: 'center', textAlign: 'center', padding: '20px' }}>
              <ViewLink href='#' style={{ textDecoration: 'none' }}>
                View Account
              </ViewLink>
            </div>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
}
