import { Typography } from '@material-ui/core';
import React from 'react';
import styled from 'styled-components';
const StyledList = styled.ul`
  padding: 2px 5px;
  list-style: none;
  margin: 0px;
`;

const ListItem = styled.li`
  color: #949aa6;
  padding: 2px 0px;
`;

function RecentJobs() {
  return (
    <div>
      <Typography variant='subtitle1' component='h1'>
        <b>Recent Job:</b>
      </Typography>
      <StyledList>
        <ListItem>Solar System installed location x</ListItem>
        <ListItem>Solar System installed in location x</ListItem>
        <ListItem>Solar System installed for location x</ListItem>
        <ListItem>Solar System installed for location x</ListItem>
      </StyledList>
    </div>
  );
}

export default RecentJobs;
