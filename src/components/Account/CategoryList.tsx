import React from 'react';
import styled from 'styled-components';

const Categories = styled.div`
  color: #949aa6;
  padding: 0px;
`;
function CategoryList() {
  return (
    <Categories>
      <p>Solar System, Guttering, Battery Storage</p>
    </Categories>
  );
}

export default CategoryList;
